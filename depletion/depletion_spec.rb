# Rspec tests for depletion prediction program
require "./recurring_use"
require "./depletion_predictor"

describe DepletionPredictor do
  
  it "returns the correct amount" do
    predictor = DepletionPredictor.new(5000)
    predictor.amount.should eq(5000)
  end
  
  it "periodicity must be daily or weekly" do
    predictor = DepletionPredictor.new(200)
    parsed = predictor.parse_usage_report("test6.txt")
    parsed.should_not be_true
  end

  it "can parse a recurring usage report" do
    predictor = DepletionPredictor.new(5000)
    parsed = predictor.parse_usage_report("test1.txt")
    bad_parsed = predictor.parse_usage_report("not a file")
    parsed.should eq(true)
    bad_parsed.should_not eq(true) 
  end

  it "builds a usage range" do
    predictor = DepletionPredictor.new(5000)
    predictor.parse_usage_report("test1.txt")
    predictor.indefinite.should_not be_true
    #test a the range of a recurring use with no end date
    predictor2 = DepletionPredictor.new(2000)            
    predictor2.parse_usage_report("test2.txt")
    predictor2.indefinite.should be_true
  end

  it "can forcast depletion for a range of dates" do
    predictor = DepletionPredictor.new(5000)
    predictor.parse_usage_report("test3.txt")
    val = predictor.depletion_prediction
    #last usage is equal to 5000
    #date should be the day before it 2014-10-25
    val.should eql( Date.new(2014,10,25) )
  end

  it "can handle recurring uses with no end date" do
    predictor = DepletionPredictor.new(5000)
    predictor.parse_usage_report("test2.txt")
    predictor.depletion_prediction
  end

  it "will return the amount in the case where all uses have reached the end" do
     predictor = DepletionPredictor.new(5000)
     predictor.parse_usage_report("test1.txt")
     report = predictor.depletion_prediction
     #should be a string not a date
     report.class.should eql("string".class)
  end

  it "can handle zero and negative cases properly" do
     predictor = DepletionPredictor.new(100)
     predictor.parse_usage_report("test4.txt")
     #Should end exactly on 12-14 and not 12-13 see test4.txt
     report = predictor.depletion_prediction
     report.should eql( Date.new(2014,12,14) )
     
     predictor2 = DepletionPredictor.new(100)     
     predictor2.parse_usage_report("test5.txt") #test3.txt with amount plus 1
     #Should end exactly on 12-13 and not 12-14
     report = predictor2.depletion_prediction
     report.should eql( Date.new(2014,12,13) )
  end
  
  it "informs user if the first chemical withdrawl results in negative balance" do
     predictor = DepletionPredictor.new(2) 
     predictor.parse_usage_report("test2.txt")
     report = predictor.depletion_prediction
     report.should eq("The chemical was depleted on the first use") 
  end

end
