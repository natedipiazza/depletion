#!/usr/bin/env ruby

require "./recurring_use"
require "./depletion_predictor"
if(ARGV.size != 2)
 puts "Usage: ./run amount <testfile>"
 exit(1)
end
predictor = DepletionPredictor.new(ARGV[0].to_i)
parsed = predictor.parse_usage_report(ARGV[1])
if(parsed)
  report = predictor.depletion_prediction
  puts report
else
 puts "Error: could not parse file"
 exit(1)
end
