#Chemical Depletion prediction algorithm based on recurring usage intervals
#Author: Nate DiPiazza

require "./recurring_use"
require "date"

class DepletionPredictor
  attr_reader :amount, :range, :indefinite

  def initialize(amount)
    @amount = amount
    @recurring_uses = []
    @range = nil
    @indefinite = false
  end
  #given a comma seperated text file
  # with the format: 
  #   amount,periodicty,start date,end date
  # date format:
  #   Y-M-Da
  #
  #this function will fill the @recurring_uses and @range
  def parse_usage_report(filename)
    begin
      lines = IO.readlines(filename)
    rescue
      return false
    end
    max_date = Date.today #init to earliest pos date
    lines.each do |line|
      entries = line.split(",")
      #0 = amount     
      #1 = periodicity {daily, weekly}
      if( (entries[1] != "daily") and (entries[1] != "weekly") )
        return false
      end
      #2 = start date
      date_segments = entries[2].split("-").map {|d| d.to_i}
      start_date = Date.new(date_segments[0], date_segments[1], date_segments[2])
      end_date = nil
      #3 = end date
      if(entries[3])
        date_segments = entries[3].split("-").map {|d| d.to_i}
        end_date = Date.new(date_segments[0], date_segments[1], date_segments[2]) unless date_segments.nil?
        if(end_date > max_date)
          max_date = end_date
        end
      else
        #if last entry nil no end date given
        #This use is considered indefinite
        @indefinite = true
      end
      #3 = end date   
      recurring_use = RecurringUse.new(entries[0], entries[1], start_date, end_date)
      @recurring_uses << recurring_use
    end
    @range = Date.today..max_date
    return true 
  end
  #After parse_usage_report has been
  #run this function will calculate
  #the forcasted depletion date.
  #If there is any amount of the chemical remaining
  #a message will be returned with the total amount,
  #else the date of depletion will be returned
  def depletion_prediction
    latest_use = "The chemical was depleted on the first use"
    if(@indefinite)
      #indefinite time period of recurring uses
      #loop through days until depletion occurs
      day = Date.today
      while(@amount > 0)
        @recurring_uses.each do |use|
          if(day >= use.start_date)
            if( use.end_date.nil? or (day < use.end_date) )
              #get period and subtract from amount
              deduct(use, day)
            end
          end
        end
        if(@amount >= 0)
          latest_use = day #update day on zero too
        end
        day = day + 1
      end
      return latest_use       
    else
      #loop through every day from today..max recurring use end date
        #for each recurring use
      @range.each do |day|
        @recurring_uses.each do |use|
          #create a date range to check for membership                                 
          use_range = use.start_date..use.end_date
          if(use_range.member?(day))         
            #get period and subtract from amount
            deduct(use, day)
          end                                   
        end
        if(@amount >= 0)
          latest_use = day #update day on zero too
        end
        if(@amount <= 0)
          return latest_use
        end
      end
      #inform user of remaining chemical
      return "After all recurring usages have been deducted, there is still #{@amount} remaining."
    end
    #should not reach here
  end
  #for recurring uses that occur on this day
  #deduct their stated amount and check if
  #weekly uses fall on the given day
  def deduct(use, day)
    if(use.periodicity.eql? "daily")
      @amount = @amount - use.amount
    #For weekly only deduct if it is the correct day of the week
    else
      if(day.wday == use.start_date.wday)
        @amount = @amount - use.amount
      end
    end
  end

end
