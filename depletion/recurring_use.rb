# RecurringUse class defines a model for typical chemical usage
# Used by Depletion class to predict chemical depletion
#Author: Nate DiPiazza
require "date"

class RecurringUse
  attr_reader :start_date, :end_date, :amount, :periodicity
  def initialize(amount, periodicity, start_date, end_date)
    @amount = amount.to_i #amount of chemical deducted on each usage interval
    @periodicity = periodicity #Either daily or weekly
    @start_date = start_date
    @end_date = end_date #optional
  end
end
