Code Problem: Depletion

To solve this problem I used ruby and created tests using rspec.
For testing purposes I also created several
text files in order to load the Recurring Use data.
The format is as follows: amount,periodicity,start date,end date
Date format: Y-M-D

I also created a script to run the depletion prediction function. 
Usage from the command line is as follows:
  ./run.rb amount <recurring use data filename>
'amount' corresponds to the total chemical amount.

The DepletionPrediction class has two main functions:

parse_usage_report-preprocessing step which loads the recurring use data

depletion_prediction-calculates and returns the forcasted delpletion date

My prefered params for rspec:

rspec depletion_spec.rb -f documentation -c


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

I hope the code meets your satisfaction. Let me know if you have any questions.

-Nate DiPiazza
